importPackage(Packages.org.csstudio.opibuilder.scriptUtil);

var W_MAX_SIZE = 1012;
var H_MAX_SIZE = 448;
var scale, W,H, rx, ry;

var X = PVUtil.getDouble(pvs[0]);
var Y = PVUtil.getDouble(pvs[1]);

rx = X/ W_MAX_SIZE;
ry = Y/H_MAX_SIZE;

      if(rx>ry){
		W=W_MAX_SIZE;
        scale = Y/X;
        H = Math.round(scale*W_MAX_SIZE);
      } else {
        H=H_MAX_SIZE;
        scale = X/Y;
        W = Math.round(scale*H_MAX_SIZE);
      }

widget.setPropertyValue("graph_area_width", W);
widget.setPropertyValue("graph_area_height", H);