<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title>Cryo SEC</title>
  <show_toolbar>true</show_toolbar>
  <update_period>5.0</update_period>
  <scroll_step>12</scroll_step>
  <scroll>true</scroll>
  <start>-30 minutes</start>
  <end>now</end>
  <archive_rescale>STAGGER</archive_rescale>
  <foreground>
    <red>0</red>
    <green>0</green>
    <blue>0</blue>
  </foreground>
  <background>
    <red>255</red>
    <green>255</green>
    <blue>255</blue>
  </background>
  <title_font>Liberation Sans|20|1</title_font>
  <label_font>Liberation Sans|14|1</label_font>
  <scale_font>Liberation Sans|12|0</scale_font>
  <legend_font>Liberation Sans|14|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>Pressure</name>
      <use_axis_name>true</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>0</blue>
      </color>
      <min>2.4429804976055346E-6</min>
      <max>2.4630316921568486E-6</max>
      <grid>true</grid>
      <autoscale>true</autoscale>
      <log_scale>true</log_scale>
    </axis>
    <axis>
      <visible>true</visible>
      <name>P</name>
      <use_axis_name>true</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>true</right>
      <color>
        <red>0</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <min>2.579942340724191E-9</min>
      <max>2.6350569147375755E-9</max>
      <grid>true</grid>
      <autoscale>true</autoscale>
      <log_scale>true</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>Diag</display_name>
      <visible>true</visible>
      <name>PINK:MAXA:S2Measure</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>255</green>
        <blue>127</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>PINK</name>
        <url>pbraw://172.17.10.46:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>HV</display_name>
      <visible>true</visible>
      <name>PINK:MAXB:S6Measure</name>
      <axis>1</axis>
      <color>
        <red>127</red>
        <green>0</green>
        <blue>255</blue>
      </color>
      <trace_type>SINGLE_LINE</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>PINK</name>
        <url>pbraw://172.17.10.46:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
  </pvlist>
</databrowser>
